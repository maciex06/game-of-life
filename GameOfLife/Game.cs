﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class Game
    {
        public readonly List<Cell> Cells;

        public Game(params Cell[] cells)
        {
            Cells = new List<Cell>(cells);
        }

        public void NextGeneration()
        {
            var CellsToKill = new List<Cell>();
            var CellsToRevive = new List<Cell>();

            foreach (Cell cell in Cells)
            {
                if (CountNeighbours(cell) < 2 || CountNeighbours(cell) > 3)
                {
                    CellsToKill.Add(cell);
                }

                for (int x = -1;x<=1;x++)
                {
                    for (int y=-1;y<=1;y++)
                    {
                        if (x==cell.X&&y==cell.Y)
                        {
                            continue;
                        }
                        var cellToCheck = new Cell(x, y);
                        if (CountNeighbours(cellToCheck)==3)
                        {
                            if (CellsToRevive.Any(c=>c.X==x&&c.Y==y)==false)
                            {
                                CellsToRevive.Add(cellToCheck);
                            }
                        }
                    }
                }
            }
            foreach (Cell cell in CellsToKill)
            {
                var cellToKill = Cells.FirstOrDefault(c => c.X == cell.X && c.Y == cell.Y);
                Cells.Remove(cellToKill);
            }
            foreach (Cell cell in CellsToRevive)
            {
                Cells.Add(cell);

            }
        }

        private int CountNeighbours(Cell cell)
        {
            int aliveNeighbours = 0;
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X - 1, cell.Y - 1);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X - 1, cell.Y);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X - 1, cell.Y + 1);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X, cell.Y - 1);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X, cell.Y + 1);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X + 1, cell.Y - 1);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X + 1, cell.Y);
            IncrementAliveNeighbours(ref aliveNeighbours, cell.X + 1, cell.Y + 1);

            return aliveNeighbours;
        }

        private void IncrementAliveNeighbours(ref int aliveNeighbours, int x, int y)
        {
            aliveNeighbours += Cells.Any(c => c.X == x && c.Y == y) ? 1 : 0;
        }
    }
}
