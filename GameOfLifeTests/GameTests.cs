﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using GameOfLife;

namespace GameOfLifeTests
{
    public class GameTests
    {
        [Test]
        public void Constuctor_Should_InicializeCellsList_When_Invoked()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 2);
            var cell2 = new Cell(0, 3);
            var cells = new List<Cell>() { cell0, cell1, cell2 };

            var game = new Game(cell0,cell1,cell2);

            Assert.AreEqual(cells, game.Cells);
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_NoNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var game = new Game(cell0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_OneNeighbour()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var game = new Game(cell0, cell1);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_LeaveCellAlive_When_TwoNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var game = new Game(cell0, cell1, cell2);

            game.NextGeneration();

            Assert.IsTrue(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_LeaveCellAlive_When_ThreeNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var game = new Game(cell0, cell2, cell3,cell3);

            game.NextGeneration();

            Assert.IsTrue(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_FourNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var game = new Game(cell0, cell1, cell2, cell3, cell4);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_FiveNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var game = new Game(cell0, cell1, cell2, cell3, cell4,cell5);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_SixNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var game = new Game(cell0, cell1, cell2, cell3, cell4, cell5, cell6);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_SevenNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var cell7 = new Cell(-1, 0);
            var game = new Game(cell0, cell1, cell2, cell3, cell4, cell5, cell6, cell7);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KillLivingCell_When_EightNeighbours()
        {
            var cell0 = new Cell(0, 0);
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var cell7 = new Cell(-1, 0);
            var cell8 = new Cell(-1, -1);
            var game = new Game(cell0, cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_NoNeighbours()
        {
            var game = new Game();
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_OneNeighbour()
        {
            var cell1 = new Cell(0, 1);
            var game = new Game(cell1);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_TwoNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var game = new Game(cell1);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_ReviveDeadCell_When_ThreeNeighbours()
        {            
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var game = new Game(cell1, cell2, cell3);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsTrue(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_FourNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var game = new Game(cell1, cell2, cell3, cell4);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_FiveNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var game = new Game(cell1, cell2, cell3, cell4, cell5);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_SixNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var game = new Game(cell1, cell2, cell3, cell4, cell5, cell6);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_SeventNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var cell7 = new Cell(-1, 0);
            var game = new Game(cell1, cell2, cell3, cell4, cell5, cell6, cell7);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }

        [Test]
        public void NextGeneration_Should_KeepDeadCellDead_When_EightNeighbours()
        {
            var cell1 = new Cell(0, 1);
            var cell2 = new Cell(0, -1);
            var cell3 = new Cell(1, 1);
            var cell4 = new Cell(1, 0);
            var cell5 = new Cell(1, -1);
            var cell6 = new Cell(-1, 1);
            var cell7 = new Cell(-1, 0);
            var cell8 = new Cell(-1, -1);
            var game = new Game(cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8);
            var cell0 = new Cell(0, 0);

            game.NextGeneration();

            Assert.IsFalse(game.Cells.Contains(game.Cells.Find(c => c.X == 0 && c.Y == 0)));
        }
    }
}
